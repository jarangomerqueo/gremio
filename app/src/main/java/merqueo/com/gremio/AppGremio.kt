package merqueo.com.gremio

import android.app.Application
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin

/**
 * Created by Jorge Henao on 19/02/21.
 */
class AppGremio : Application() {

    override fun onCreate() {
        super.onCreate()
        // Start Koin
        startKoin {
            androidLogger()
            androidContext(this@AppGremio)
            modules(
                listOf(
                    apiModule,
                    viewModel,
                    useCaseModule,
                    repositoryModule
                )
            )
        }
    }
}
