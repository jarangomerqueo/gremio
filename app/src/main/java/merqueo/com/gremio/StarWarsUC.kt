package merqueo.com.gremio

class StarWarsUC(private val starWarsRepository: StarWarsRepository) {

   fun getPeople() = starWarsRepository.getPeople()
}