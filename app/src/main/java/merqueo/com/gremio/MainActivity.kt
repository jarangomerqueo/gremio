package merqueo.com.gremio

import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.lifecycleScope
import kotlinx.coroutines.flow.collect
import org.koin.android.viewmodel.ext.android.viewModel

class MainActivity : AppCompatActivity() {

    private val starWarsViewModel: StarWarsViewModel by viewModel()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        initObservers()
        starWarsViewModel.getPeople()
    }

    private fun initObservers() {
        lifecycleScope.launchWhenCreated {
            starWarsViewModel.response.collect {
                when (it) {
                    StarWarsState.Success -> {
                        Toast.makeText(this@MainActivity, "Proceso Exitoso", Toast.LENGTH_SHORT).show()
                    }
                    StarWarsState.Failure -> {
                        Toast.makeText(this@MainActivity, "Daniel la cago!", Toast.LENGTH_SHORT).show()
                    }
                    StarWarsState.Empty -> {
                        //empty
                    }
                }
            }
        }
    }
}
