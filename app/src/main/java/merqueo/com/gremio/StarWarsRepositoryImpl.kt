package merqueo.com.gremio

import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.flow
import okhttp3.ResponseBody

class StarWarsRepositoryImpl(private val api: StarWarsApi) : StarWarsRepository {
    override fun getPeople(): Flow<ResponseBody> = flow {
        val response = api.getPeople()
        emit(response)
    }.catch { cause: Throwable ->
        println("error: ${cause.message}")
    }
}