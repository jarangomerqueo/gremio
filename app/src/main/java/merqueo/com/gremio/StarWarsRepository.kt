package merqueo.com.gremio

import kotlinx.coroutines.flow.Flow
import okhttp3.ResponseBody

interface StarWarsRepository {
    fun getPeople(): Flow<ResponseBody>
}