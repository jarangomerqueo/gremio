package merqueo.com.gremio

import kotlinx.coroutines.flow.MutableStateFlow
import merqueo.com.gremio.data.RetrofitBuild
import org.koin.android.viewmodel.dsl.viewModel
import org.koin.core.module.Module
import org.koin.dsl.module

/**
 * Created by Jorge Henao on 19/02/21.
 */
val apiModule: Module = module {
    factory {
        RetrofitBuild.getSupplyInstance().create(StarWarsApi::class.java)
    }
}

val viewModel: Module = module {
    viewModel {
        StarWarsViewModel(
            starWarsUC = get(),
            _response = MutableStateFlow(StarWarsState.Empty)
        )
    }
}

val useCaseModule: Module = module {
    factory { StarWarsUC(starWarsRepository = get()) }
}

val repositoryModule: Module = module {
    factory<StarWarsRepository> { StarWarsRepositoryImpl(api = get()) }
}
