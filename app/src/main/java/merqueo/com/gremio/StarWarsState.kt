package merqueo.com.gremio

/**
 * Created by Jorge Henao on 19/02/21.
 */
sealed class StarWarsState {
    object Success: StarWarsState()
    object Failure: StarWarsState()
    object Empty: StarWarsState()
}
