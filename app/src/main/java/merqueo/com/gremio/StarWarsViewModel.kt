package merqueo.com.gremio

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch

/**
 * Created by Jorge Henao on 19/02/21.
 */
class StarWarsViewModel(
        private val starWarsUC: StarWarsUC,
        private val _response: MutableStateFlow<StarWarsState>
) : ViewModel() {

    val response: StateFlow<StarWarsState>
        get() = _response

    fun getPeople() {
        viewModelScope.launch {
            starWarsUC.getPeople().catch {
                _response.value = StarWarsState.Failure
            }.collect {
                _response.value = StarWarsState.Success
            }
        }
    }
}
