package merqueo.com.gremio

import okhttp3.ResponseBody
import retrofit2.http.GET

/**
 * Created by Jorge Henao on 19/02/21.
 */
interface StarWarsApi {

    @GET("people/3/")
    suspend fun getPeople(): ResponseBody

}
